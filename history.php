<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js" integrity="sha384-lpyLfhYuitXl2zRZ5Bn2fqnhNAKOAaM/0Kr9laMspuaMiZfGmfwRNFh8HlMy49eQ" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <title>Document</title>
    <link rel="stylesheet" href="styles/history.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
        <a class="nav-link" href="create.php">Create User</a>
        <a class="nav-link" href="#">Show Users</a>
        <a class="nav-link" href="#">Transaction History</a>
        
      </div>
    </div>
  </div>
</nav>

<div class="circle"></div>
<div class="container">
  
<div class="row row-cols-1 row-cols-sm-2 row-cols-md-4 bg-success">
    <div class="col-md-1" style="color:yellow">Id</div>
    <div class="col">Sender</div>
    <div class="col">Receiver</div>
    <div class="col">Amount</div>
    <div class="col">Time</div>
    <div class="col">Status</div>
</div>
<?php 
        include 'connection.php';
        $sql = "SELECT * FROM transact";
        $result = $conn->query($sql);
        
            if ($result->num_rows > 0) {
              $id=1;
                // output data of each row
                while($row = $result->fetch_assoc()) {
                 
                  ?>
  <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
    <div class="col-md-1"><?php echo $id;?></div>
    <div class="col"><?php echo $row['sender'];?></div>
    <div class="col"><?php echo $row['receiver'];?></div>
    <div class="col"><?php echo "₹".$row['amt'];?></div>
    <div class="col"><?php echo $row['time'];?></div>
    <div class="col"><?php echo $row['status'];?></div>
    
    <!-- <div class="col"><a href=""><i class="fas fa-credit-card"></i></a></div> -->
    
  </div>
  <?php
  $id=$id+1;
                }}
                ?>
</body>
</html>


<!-- <?php
    include 'connection.php';
    if (isset($_POST['info'])){
    $ids = $_GET['Name'];
    ?>
    <script>
      alert("<?php echo $ids;?>");
    </script>
    <?php
    }
?> -->